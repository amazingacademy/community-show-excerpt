# name: show-excerpt
# about: A super simple plugin to demonstrate how plugins work
# version: 0.0.1
# authors: Mike K

after_initialize do

 require 'listable_topic_serializer'

 class ::ListableTopicSerializer

   def include_excerpt?
     true
   end

 end

end